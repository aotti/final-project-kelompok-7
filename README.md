# Final Project

## Kelompok 7

Anggota Kelompok:
- Fattah Jundi
- Generik Syahputra
- Hendra Saktiyasa

## Tema Project

Forum Tanya Jawab

## ERD

![ERD Forum Tanya Jawab](/public/img/erd_fix.png?raw=true)

## Link Video

video oleh Generik Syahputra
https://www.youtube.com/watch?v=axS4Ki8MFGU
